const puppeteer = require('puppeteer')
const pages = require('../.cache/pages.json')

async function processPage(page) {
  await page.$eval('#back', el => (el.style.display = 'none'))
  await page.$eval('header', el => {
    const h1 = el.getElementsByTagName('h1')[0]
    h1.style.marginTop = '.3em'
    h1.style.fontSize = '2.6em'
    el.style.fontSize = '1.5rem'
    el.style.minHeight = '13.35em'
  })
}

async function screenshot(path) {
  const browser = await puppeteer.launch()
  const page = await browser.newPage()
  const slug = path.replace('/', '')
  const file = `${__dirname}/../public/preview-${slug}.png`

  await page.goto(`http://0.0.0.0:8000${path}`)
  await page.setViewport({
    width: 1200,
    height: 630,
  })
  await processPage(page)
  await page.screenshot({ path: file })

  await browser.close()
}

pages
  .filter(
    page => page.componentChunkName === 'component---src-templates-post-js'
  )
  .forEach(page => screenshot(page.path))
