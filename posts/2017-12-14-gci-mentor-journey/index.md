---
title: Journey from Besut Kode Student to Google Code-in Mentor
date: "2017-12-14T09:36:57.526Z"
slug: "/gci-mentor-journey"
---

It all began in 2016 when my friend told me about [Besut Kode][bk]. That year, I
thought Besut Kode was a competitive programming contest in Indonesia that
involves skills in algorithms and mathematics. I'm not really interested in that
kind of competition, so I didn't care about it. Yeah, ignorance is bliss here.
Besut Kode was, in fact not like that, read on.

In the same year, I have participated in Lomba Kompetensi Siswa (LKS; translate:
Students Skill Competition) on Web Design and Development field, It's a national
scale hackathon. I failed on province level, but I felt very close on getting to
the national. So, I planned to participate in next year (2017) hoping I would
make it to national. My mind was only focused on that at that point.

Along the way, I saw <!-- prettier-ignore -->[@yuki\_is\_bored][yuki] posted
some updates on his Facebook about some of his activity in Besut Kode. I already
knew him from 2-3 years ago from [Minecraft](https://minecraft.net), although
our interaction is very minimal to be considered "a friend". After seeing some
of his post, I still didn't have any interest in Besut Kode or the need to seek
more information about it.

<!-- prettier-ignore -->
Early 2017, I saw [@scottms][scott] (already knew him; same as Yuki),
[@yuki\_is\_bored][yuki] and [@raefaldhia][raef] (didn't know him at that time)
won **Grand Prize** on
[Google Code-in](https://codein.withgoogle.com/archive/2016/). There's also
other participants from Indonesia that made it into finalist. Then, I discovered
that all of them are Besut Kode students (except [@scottms][scott]). Before
that, I didn't know what [Google Code-in][gci] was and that Besut Kode had any
connection to it .

I checked back the
[Besut Kode's website](http://besutkode.org/archive/besutkode2016/beranda/),
read it thoroughly this time. Then, I realized it was all about open source, not
competitive programming. Although the wording is a bit misleading there and it
did contain some tasks that are close to what competitive programming has, but
not quite ([Besut Kode 2017][bk] is on point).

The fact is, Besut Kode is not a competition, it's a mentoring and training
program to prepare for Google Code-in and also open source software in general,
even though it said "Competition" in the website. At the end of Besut Kode, the
mentors will choose potential participants (not one) to participate in Google
Code-in and Besut Kode mentors will help them along the way. The rest of the
participants can join Google Code-in on their own, no problem whatsoever.

Next, I checked what [Google Code-in](gci) is all about. It's a contest, that
introduces pre-university students to open source. I'm immediately interested.
Until it said it's only for age 13-17, and I'm already 17 years old on 2016.

I slowly began doubting myself whether or not I could participate in Google
Code-in 2017. I might still have a chance to be able to participate since my
birthday is in November, that's what I was thinking back then. Anyway, I didn't
think too much about it, since LKS 2017 is near (_spoiler_: I still didn't get
to national).

August 2017, Besut Kode started their campaign of their 2017 program, I'm also
interested in participating, I already lose my interest in LKS at that point. I
saw [@scottms][scott] shared it in his Facebook profile and saw him as a mentor
as well. I asked him whether or not I could participate while mentioning my
birthday is November 1999, and **he replied no**.

But, I did try to register anyway. That's when I met [@jayvdb][john], the lead
of Besut Kode program. I asked him about my age condition. He said yes, I could
participate in Besut Kode, with warning that Google Code-in is very strict on
age. We'll cross that bridge when we come to it, I guess.

September-October, I was busy doing Besut Kode tasks (tasks are
[here](https://github.com/BesutKode/sma2017-tugas); written in Bahasa, if
anyone's interested). I met some interesting people in Besut Kode that has the
same interests in open source software as me. The tasks is not very hard, but I
think it was challenging enough and I think that's the important part of getting
the participants excited.

Then came the end of the road. Google announced that the contest will start on
29th November, with a rule that on the date of registration, the participants
must be 17 years old or less. Yes, I saw it coming and I pushed my luck too far
here. I can't believe it's **only 21 days after my birthday**.

When I chatted about it in Besut Kode's chatroom, [@jayvdb][john] sent me a
private message

> How about becoming a mentor for GCI?\
> Instead of being a participant, be a mentor.

That message, makes me a bit better after seeing that Google Code-in start date.
All the effort that I do to make for Google Code-in by participating in Besut
Kode didn't go to waste after all.

[@jayvdb][john] is one of [coala][coala]'s administrator, so he told me to do
[coala's newcomer process](https://coala.io/newcomer) and make sure to finish it
before 29th November so I could be a developer at coala and then mentor in
Google Code-in.

So, I did the newcomer process successfully and without too much trouble. Thanks
to [@jayvdb][john], he helped me a lot through the process.

I know being a mentor is not always a good thing. I might get more tasks to do
(reviewing) and hold a greater responsibility compared to participants. Mentors
are volunteers and they only get a t-shirt after the competition ends as a thank
you.

But, personally I was more interested in the learning experience and a chance of
being acknowledged by the open source community, which I was both lacking in the
first place. Besides, what can I do? Graduate as a Besut Kode student and do
nothing after that? Being a mentor is my only choice here.

So here I am, a Google Code-in mentor for coala organization. What mentors do is
to create tasks and review the work done by the participants, mainly doing code
review. Here at coala, I do mentoring on JavaScript related tasks.

We already made 2 new things:

* [Google Code-in leaders](https://gci-leaders.netlify.com), a website to show
  all leaders from all organizations
  [![GitBub repository](/images/github.svg)](https://github.com/coala/gci-leaders)
* [coala community website](https://community.coala.io)
  [![GitBub repository](/images/github.svg)](https://github.com/coala/community)

Although, [@jayvdb][john] was the one that made 80% of the tasks and he did some
mentoring as well, I still have my part.

Aside from the new stuff, we have many improvements and bug fixes done by Google
Code-in students in many existing [coala's projects][coala].

So there you have it, here's a quote from [@jayvdb][john] to sum it up.

> The boy who missed his dream by 21 days and got a lot more work instead.\
> —[@jayvdb][john]

If you're a high school student age 13-17, don't hesitate to check out
[Google Code-in](https://codein.withgoogle.com/) and if you're Indonesian please
participate in [Besut Kode][bk], they have a program for university students
too. Also, don't make the same mistake that I do 😋

[bk]: http://besutkode.org/
[gci]: https://codein.withgoogle.com/
[yuki]: https://github.com/yukiisbored
[scott]: https://github.com/smsunarto
[raef]: https://github.com/raefaldhia
[john]: https://github.com/jayvdb
[coala]: https://github.com/coala
