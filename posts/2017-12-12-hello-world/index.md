---
title: Hello World
date: "2017-12-12T08:00:00.048Z"
slug: "/hello-world"
---

_Hello World!_

I seriously don't know what to put here. This website is here because I bought
the domain and I don't want it go to waste. Let's just talk about the nuts and
bolts about this website in this post because I don't have another idea.

I'm using [GatsbyJS][1] to build this blog. Since I'm new to that, I'm following
this [Creating a Blog with Gatsby][2] and [Migrate from Hugo to Gatsby][3]
tutorial, although I don't really migrate from Hugo, it still contains some
useful bits that I can learn and then use.

GatsbyJS uses [React][4], a JavaScript library for building user interfaces. I
used it before, so the concept is not that new to me. The result is a
Single-Page Application (SPA), with navigation handled in the client browser.
That is why the navigation feels different _(and fast!)_.

A SPA usually does not work without JavaScript enabled, because it relies on
rendering the page in the browser with JavaScript, but this one does fallback
nicely without JavaScript enabled. It works by generating static version of
every page on build time, just like old-school static site generator.

As a bonus, it even works without an internet connection. Try refreshing this
page without an internet connection. Usually, setting up a stuff like that takes
a while and might not even be worth your time. But GatsbyJS has a [plugin for
that][5] and it's as simple as adding one-liner to the config file to enable it,
_very neat_.

[1]: https://www.gatsbyjs.org
[2]: https://www.gatsbyjs.org/blog/2017-07-19-creating-a-blog-with-gatsby
[3]: https://www.gatsbyjs.org/blog/2017-11-06-migrate-hugo-gatsby
[4]: https://reactjs.org
[5]: https://github.com/gatsbyjs/gatsby/tree/master/packages/gatsby-plugin-offline
