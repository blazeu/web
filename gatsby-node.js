const createPostPages = require(`./gatsby-actions/createPostPages`)
const createPaginatedPostsPages = require(`./gatsby-actions/createPaginatedPostsPages`)

exports.createPages = ({ graphql, boundActionCreators }) => {
  const { createPage } = boundActionCreators
  graphql(`
    {
      allMarkdownRemark(sort: { fields: [frontmatter___date], order: DESC }) {
        edges {
          node {
            frontmatter {
              title
              slug
              date
            }
          }
        }
      }
    }
  `).then(result => {
    const posts = result.data.allMarkdownRemark.edges
    createPostPages(createPage, posts)
    createPaginatedPostsPages(createPage, posts)
  })
}
