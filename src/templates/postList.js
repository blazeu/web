import React from 'react'
import Link from 'gatsby-link'
import format from 'date-fns/format'
import Header from '../components/Header'
import Footer from '../components/Footer'
import Container from '../components/Container'

import styles from './css/post-list.module.css'

const Timestamp = ({ date }) => {
  const long = format(date, 'D MMMM YYYY')
  const dateMonth = format(date, 'D/M')
  const year = format(date, 'YYYY')

  return (
    <div>
      <time dateTime={date} className={styles.longdate}>
        {long}
      </time>
      <time dateTime={date} className={styles.shortdate}>
        {dateMonth}
      </time>
      <time
        dateTime={date}
        className={[styles.shortdate, styles.dateyear].join(' ')}
      >
        {year}
      </time>
    </div>
  )
}

const Post = ({ node }) => (
  <Link className={styles.post} to={node.frontmatter.slug}>
    <Timestamp date={node.frontmatter.date} />
    <p>{node.frontmatter.title}</p>
  </Link>
)

const BlogPagedIndex = ({ pathContext }) => {
  const { group, index, first, last } = pathContext
  return (
    <div>
      <Header>
        <h1>Hi, I'm Surya Widi</h1>
        <p>This is where I write some stuff</p>
      </Header>

      <Container>
        <div className={styles.posts}>
          {group.map((node, key) => <Post key={key} node={node.node} />)}
        </div>

        <div className={styles.pagination}>
          {!last && <Link to={`/blog/${index + 1}`}>&laquo; Older posts </Link>}
          {!first && (
            <Link to={`/blog/${index > 2 ? index - 1 : ''}`}>
              Newer posts &raquo;
            </Link>
          )}
        </div>
      </Container>

      <Footer />
    </div>
  )
}

export default BlogPagedIndex
