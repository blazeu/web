import React from 'react'
import twemoji from 'twemoji'
import format from 'date-fns/format'
import Head from '../components/Head'
import Header from '../components/Header'
import Container from '../components/Container'
import styles from './css/post.module.css'
import get from 'lodash.get'

const Post = ({ data }) => {
  const post = data.markdownRemark
  const siteName = data.site.siteMetadata.title
  const siteUrl = data.site.siteMetadata.siteUrl
  const slug = post.frontmatter.slug.replace('/', '')
  const image =
    get(post, 'frontmatter.thumbnail.childImageSharp.resize.src') ||
    `/preview-${slug}.png`
  const body = twemoji.parse(post.html, { className: styles.emoji })

  return (
    <div>
      <Head title={post.frontmatter.title}>
        <meta property="og:type" content="article" />
        <meta
          property="og:url"
          content={`${siteUrl}${post.frontmatter.slug}`}
        />
        <meta property="og:title" content={post.frontmatter.title} />
        <meta property="og:site_name" content={siteName} />
        <meta property="og:image" content={siteUrl + image} />
        <meta property="og:image:width" content="1200" />
        <meta property="og:image:height" content="630" />
        <meta
          property="article:author"
          content="https://www.facebook.com/suryawidi"
        />
        <meta
          property="article:published_time"
          content={post.frontmatter.date}
        />
      </Head>

      <Header back>
        <h1>{post.frontmatter.title}</h1>
        <p>
          <time dateTime={post.frontmatter.date}>
            {format(post.frontmatter.date, 'D MMMM YYYY')}
          </time>
        </p>
      </Header>

      <div className={styles.post}>
        <Container>
          <div
            className={styles.body}
            dangerouslySetInnerHTML={{ __html: body }}
          />
        </Container>
      </div>
    </div>
  )
}

export const query = graphql`
  query PostPage($slug: String!) {
    site {
      siteMetadata {
        title
        siteUrl
      }
    }
    markdownRemark(frontmatter: { slug: { eq: $slug } }) {
      frontmatter {
        title
        date
        slug
        thumbnail {
          childImageSharp {
            resize(width: 1200, height: 630, quality: 90) {
              src
            }
          }
        }
      }
      html
    }
  }
`

export default Post
