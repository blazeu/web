import React from 'react'

import styles from './css/container.module.css'

const Container = props => (
  <div {...props} className={[styles.container, ...props.className].join(' ')}>
    {props.children}
  </div>
)

export default Container
