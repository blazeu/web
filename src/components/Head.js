import React from 'react'
import Helmet from 'react-helmet'

const Head = ({ title, children }) => (
  <Helmet>
    <title>{title ? `${title} | ` : ''}Surya Widi</title>
    {children}
  </Helmet>
)

export default Head
