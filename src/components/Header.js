import React from 'react'
import Link from 'gatsby-link'
import Container from './Container'

import styles from './css/header.module.css'

const Back = () => (
  <Link to="/blog" className={styles.back} id="back">
    &lsaquo;
  </Link>
)

const Header = ({ children, back }) => (
  <header className={styles.header}>
    {back ? <Back /> : ''}
    <Container>{children}</Container>
  </header>
)

export default Header
