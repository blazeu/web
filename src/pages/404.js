import React from 'react'
import Link from 'gatsby-link'

import styles from './css/index.module.css'

const NotFoundPage = () => (
  <div className={styles.index}>
    <h1 className={styles.heading} style={{ textAlign: 'center' }}>
      Can't find your page ...
    </h1>
    <p style={{ textAlign: 'center' }}>
      <Link to="/">Start over?</Link>
    </p>
  </div>
)

export default NotFoundPage
