import React from 'react'
import Link from 'gatsby-link'

import styles from './css/index.module.css'

const IndexPage = () => (
  <div className={styles.index}>
    <h1 className={styles.heading}>Hi there, my name is Surya Widi</h1>
    <p>
      I'm a software developer and <i>mostly</i> likes to do web development
      stuff.
      <br className="hide-mobile" /> Go visit my <Link to="/blog">Blog </Link>{' '}
      or my <a href="https://github.com/blazeu">GitHub profile</a>.
    </p>

    <p />
  </div>
)

export default IndexPage
